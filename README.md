# Kubernetes Infrastructure as Code

Terraform is executed via Terraform Cloud on every commit that has changes within the terraform folder.
Variables need to be set in the web interface when new ones are introduced.

## How to use

Enter your hosts to set up in ansible/inventory.

Things to note:

* HAProxy will be installed on the init machine by default. To change this add a seperate host in the inventory and remove the children definition.
* If some of the nodes are already provisioned create the following files in their home directories. This will prevent running the init/join commands on them.
  * cluster_joined <- for all nodes in the cluster
  * pod_network <- on the init master

To set up the cluster run ansible like this:

```bash
pipenv install
pipenv shell
cd ansible
ansible-playbook -i inventory playbook.yml
```
