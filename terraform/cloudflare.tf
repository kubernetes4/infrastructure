provider "cloudflare" {
  email = var.cloudflare_mail_finn
  token = var.cloudflare_token_finn
  version = "~> 1.9"
}

resource "cloudflare_record" "Ronie" {
  domain = "finnhuhne.de"
  name = "ronie.k8s"
  type = "A"
  value = hcloud_server.Ronie.ipv4_address
}

resource "cloudflare_record" "Ronie-ipv6" {
  domain = "finnhuhne.de"
  name = "ronie.k8s"
  type = "AAAA"
  value = "${hcloud_server.Ronie.ipv6_address}1"
}

resource "cloudflare_record" "Selka" {
  domain = "finnhuhne.de"
  name = "selka.k8s"
  type = "A"
  value = hcloud_server.Selka.ipv4_address
}

resource "cloudflare_record" "Selka-ipv6" {
  domain = "finnhuhne.de"
  name = "selka.k8s"
  type = "AAAA"
  value = "${hcloud_server.Selka.ipv6_address}1"
}
