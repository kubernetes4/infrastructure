provider "hcloud" {
  token = var.hcloud_token_finn
  version = "~> 1.6"
}

resource "hcloud_ssh_key" "finn" {
  name = "finn"
  public_key = file("ssh_keys/finn.pub")
}

resource "hcloud_server" "Ronie" {
  name = "Ronie"
  image = "debian-10"
  server_type = "cx21"
  location = var.hcloud_location
  ssh_keys = ["finn"]
}

resource "hcloud_rdns" "Ronie" {
  server_id = hcloud_server.Ronie.id
  ip_address = hcloud_server.Ronie.ipv4_address
  dns_ptr = "ronie.k8s.finnhuhne.de"
}

resource "hcloud_rdns" "Ronie-ipv6" {
  server_id = hcloud_server.Ronie.id
  ip_address = "${hcloud_server.Ronie.ipv6_address}1"
  dns_ptr = "ronie.k8s.finnhuhne.de"
}

resource "hcloud_server" "Selka" {
  name = "Selka"
  image = "debian-10"
  server_type = "cx21"
  location = var.hcloud_location
  ssh_keys = ["finn"]
}

resource "hcloud_rdns" "Selka" {
  server_id = hcloud_server.Selka.id
  ip_address = hcloud_server.Selka.ipv4_address
  dns_ptr = "selka.k8s.finnhuhne.de"
}

resource "hcloud_rdns" "Selka-ipv6" {
  server_id = hcloud_server.Selka.id
  ip_address = "${hcloud_server.Selka.ipv6_address}1"
  dns_ptr = "selka.k8s.finnhuhne.de"
}
